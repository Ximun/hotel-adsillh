-- Chambre --

INSERT INTO Hotel2019.Chambre
	VALUES(200, 150, 'La chambre dans laquelle tout se passe bien.');
INSERT INTO Hotel2019.Chambre
	VALUES(301, 30, 'Une fois installé dans cette chambre, attendez-vous à être redirigé ailleurs...');
INSERT INTO Hotel2019.Chambre
	VALUES(404, 50, 'Malheureseument, vous ne trouverez jamais ce que vous cherchez dans cette chambre...');
INSERT INTO Hotel2019.Chambre
	VALUES(500, 50, 'Excusez-nous, tout ce qui ne se passera pas comme prévu ici est entièrement de notre faute...');

-- Client --

/*
INSERT INTO Hotel2019.Client
	VALUES(1, 'jean', 'claude', 'jean@claude.fr', '7c37be7260f8cd7c1f5e4dbdd7bc5b23');
INSERT INTO Hotel2019.Client
 VALUES(2, 'marie', 'jeanne', 'marie@jeanne.fr', '7c37be7260f8cd7c1f5e4dbdd7bc5b23');
INSERT INTO Hotel2019.Client
 VALUES(3, 'to', 'to', 'to@to.fr', '7c37be7260f8cd7c1f5e4dbdd7bc5b23');
INSERT INTO Hotel2019.Client
 VALUES(4, 'tu', 'tu', 'tu@tu.fr', '7c37be7260f8cd7c1f5e4dbdd7bc5b23');
*/
-- Reservation --

/*
INSERT INTO Hotel2019.Reservation
	VALUES(1, 200, 1, '2019-11-15', '2019-11-17');
INSERT INTO Hotel2019.Reservation
	VALUES(2, 200, 1, '2019-12-15', '2019-12-17');
INSERT INTO Hotel2019.Reservation
	VALUES(3, 301, 2, '2019-12-20', '2019-11-30');
INSERT INTO Hotel2019.Reservation
	VALUES(4, 501, 3, '2019-12-20', '2019-11-30');
INSERT INTO Hotel2019.Reservation
	VALUES(5, 404, 3, '2019-12-26', '2019-12-30');
*/

-- Bar --

INSERT INTO Hotel2019.Bar
	VALUES(DEFAULT, 'bière', 2.5);
INSERT INTO Hotel2019.Bar
	VALUES(DEFAULT, 'olives', 3);
INSERT INTO Hotel2019.Bar
	VALUES(DEFAULT, 'verre de vin', 5);

-- Descriptions --

INSERT INTO Hotel2019.Description
	VALUES(DEFAULT, 'Bienvenue dans l hôtel ADSILLH. Vous pourrez vous reposer ici tranquillement entre deux conventions sur le logiciel libre.');
INSERT INTO Hotel2019.Description
	VALUES(DEFAULT, 'Une fois votre chambre réservée, n hésitez pas à vous entraîner à compiler votre noyau Linux grâce à nos excellents professeurs, sur place et à votre disposition, ou à potasser la dernière édition de Linux From Scratch dans notre bibliothèque...');
INSERT INTO Hotel2019.Description
	VALUES(DEFAULT, 'Une réclamation à faire ou une idée d amélioration? N hésitez pas à nous envoyer vos pull requests!');
INSERT INTO Hotel2019.Description
	VALUES(DEFAULT, 'Et bien sûr, si cet hôtel vous plaît et que vous voulez vous aussi vous lancer dans l aventure,  n hésitez pas à en forker un prêt de chez vous...');
