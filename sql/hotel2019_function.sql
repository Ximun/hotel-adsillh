-- PostgreSQL n'accepte pas les contraintes d'intégrité avec un SELECT
-- Il faut définir une fonction

CREATE FUNCTION Hotel2019.DatesDebutFinCorrectes(integer, integer, date, date)
	RETURNS boolean AS $$
SELECT NOT EXISTS (
	SELECT *
	FROM Hotel2019.Reservation
	WHERE $1 != id
		AND $2 = id_chambre
		AND $3 <= date_fin
		AND $4 >= date_debut)
$$ LANGUAGE SQL;

-- Vérifie qu'une chambre est disponible au moment même
CREATE FUNCTION Hotel2019.ChambreDisponible(integer)
	RETURNS boolean AS $$
SELECT NOT EXISTS (
	SELECT *
	FROM Hotel2019.Reservation
	WHERE $1 = id_chambre
		AND NOW() >= date_debut
		AND NOW() < date_fin)
$$ LANGUAGE SQL;

-- Vérifie qu'une chambre est disponible sur une période donnée
CREATE FUNCTION Hotel2019.PeriodeLibre(integer, date, date)
	RETURNS boolean AS $$
SELECT EXISTS (
	SELECT *
	FROM Hotel2019.Chambre
	INNER JOIN Hotel2019.Reservation
	ON Hotel2019.Chambre.numero = Hotel2019.Reservation.id_chambre
	WHERE Hotel2019.Reservation.id_chambre = $1
		AND (
			$2 > Hotel2019.Reservation.date_fin AND $2 > Hotel2019.Reservation.date_debut)
		OR (
			$3 < Hotel2019.Reservation.date_fin AND $3 < Hotel2019.Reservation.date_fin)
		)
$$ LANGUAGE SQL;

-- Vérifie lors d'une entrée de consommation dans la table:
-- la commande est bien dans la plage de dates d'une réservation
-- si la facture n'est pas déjà payée
CREATE FUNCTION Hotel2019.ConsoPossible(integer, date)
	RETURNS boolean AS $$
SELECT EXISTS (
	SELECT *
	FROM Hotel2019.Reservation
	WHERE $1 = id
		AND $2 >= date_debut
		AND $2 <= date_fin
		AND paye = '0')
$$ LANGUAGE SQL;

-- Vérifie lors d'un post de commentaire:
-- que la date est bien supérieure à la fin de la réservation
CREATE FUNCTION Hotel2019.CommentairePossible(integer, date)
	RETURNS boolean AS $$
SELECT EXISTS (
	SELECT *
	FROM Hotel2019.Reservation
	WHERE $1 = id
		AND $2 >= date_fin)
$$ LANGUAGE SQL;

-- Retourne le prix total des consommations effectuées pour une réservation
CREATE FUNCTION Hotel2019.TotalConso(integer)
	RETURNS numeric AS $$
SELECT sum(nombre * tarif)
	FROM hotel2019.Consommation
	INNER JOIN hotel2019.Bar ON hotel2019.Consommation.id_conso = hotel2019.Bar.id
	WHERE id_resa = $1
$$ LANGUAGE SQL;
