CREATE VIEW hotel2019.Facture
AS SELECT Reservation.id, id_client, numero, prenom, nom, email, date_debut, date_fin, paye, ((date_fin - date_debut)*nuitee) AS nuitees, Hotel2019.TotalConso(Reservation.id) AS consos, (((date_fin - date_debut)*nuitee) + COALESCE(Hotel2019.TotalConso(Reservation.id),0)) AS total
FROM hotel2019.Reservation
	INNER JOIN hotel2019.Client ON hotel2019.Client.id = hotel2019.Reservation.id_client
	INNER JOIN hotel2019.Chambre ON hotel2019.Chambre.numero = hotel2019.Reservation.id_chambre
WHERE date_fin <= CURRENT_DATE
GROUP BY Reservation.id, id_client, numero, prenom, nom, email, date_debut, date_fin, paye, nuitee, consos, total;
