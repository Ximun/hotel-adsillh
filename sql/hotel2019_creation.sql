CREATE SCHEMA Hotel2019;
SET search_path TO Hotel2019, public;

CREATE TABLE Hotel2019.Client (
	id serial NOT NULL,
	prenom varchar NOT NULL,
	nom varchar NOT NULL,
	email varchar NOT NULL,
	pass varchar NOT NULL,
	-- clefs candidates
	PRIMARY KEY (id),
	UNIQUE (id),
	UNIQUE (email)
);

CREATE TABLE Hotel2019.Chambre (
	numero integer NOT NULL,
	nuitee decimal NOT NULL,
	presentation text NOT NULL,
	-- clefs candidates
	PRIMARY KEY (numero),
	UNIQUE (numero),
	-- contraintes élémentaires
	CHECK (nuitee >= 0)
);

CREATE TABLE Hotel2019.Reservation (
	id serial NOT NULL,
	id_chambre serial NOT NULL,
	id_client serial NOT NULL,
	date_debut date NOT NULL,
	date_fin date NOT NULL,
	paye boolean NOT NULL DEFAULT FALSE,
	-- clefs candidates
	PRIMARY KEY (id),
	UNIQUE (id),
	-- clefs etrangeres
	FOREIGN KEY (id_chambre) REFERENCES Hotel2019.Chambre(numero),
	FOREIGN KEY (id_client) REFERENCES Hotel2019.Client(id),
	-- contraintes élémentaires
	CHECK (date_fin > date_debut)
);

CREATE TABLE Hotel2019.Comment (
	id serial NOT NULL,
	id_resa serial NOT NULL,
	content text NOT NULL,
	date_ecriture date NOT NULL,
	-- clefs candidates
	PRIMARY KEY (id),
	UNIQUE (id),
	-- clefs etrangeres
	FOREIGN KEY (id_resa) REFERENCES Hotel2019.Reservation(id) ON DELETE CASCADE
);

CREATE TABLE Hotel2019.Bar (
	id serial NOT NULL,
	consommation varchar NOT NULL,
	tarif decimal NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (id),
	UNIQUE (consommation),
	CHECK (tarif >= 0)
);

CREATE TABLE Hotel2019.Consommation (
	id serial NOT NULL,
	id_conso serial NOT NULL,
	id_resa serial NOT NULL,
	date_conso date NOT NULL,
	nombre integer NOT NULL,
	-- clefs candidates
	PRIMARY KEY (id),
	UNIQUE (id),
	-- clefs etrangeres
	FOREIGN KEY (id_conso) REFERENCES Hotel2019.Bar(id),
	FOREIGN KEY (id_resa) REFERENCES Hotel2019.Reservation(id) ON DELETE CASCADE
);

CREATE TABLE Hotel2019.Description (
	id serial NOT NULL,
	description text NOT NULL,
	-- clefs candidates
	PRIMARY KEY (id),
	UNIQUE (id)
);
