-- contrainte de dates possibles sur une réservation
ALTER TABLE Hotel2019.Reservation ADD CONSTRAINT DatesPossibles
	CHECK(Hotel2019.DatesDebutFinCorrectes(id, id_chambre, date_debut, date_fin));

-- contrainte sur la commande de consommations
ALTER TABLE Hotel2019.Consommation ADD CONSTRAINT PossibiliteConsommation
	CHECK(Hotel2019.ConsoPossible(id_resa, date_conso));

-- contrainte sur le post d'un commentaire
ALTER TABLE Hotel2019.Comment ADD CONSTRAINT DateEcritureSuperieureDateSejour
	CHECK(Hotel2019.CommentairePossible(id_resa, date_ecriture));
