from flask import flash
from database import select_name_by_client, check_dispo, select_room

def error(e):
  flash("ERREUR: " + str(e), "danger")

def construct_user(user):
  if len(user) == 0:
    return False
  else:
    return {
      "id": user[0][0],
      "prenom": user[0][1],
      "nom": user[0][2],
      "email": user[0][3]
    }

def construct_rooms(rooms):
  if len(rooms) == 0:
    return False
  else:
    result = []
    for room in rooms:
      result.append({
        "numero": room[0],
        "nuitee": room[1],
        "presentation": room[2],
        "dispo": check_dispo(room[0])[0][0]
      })
    return result

def construct_rooms_on_ids(ids):
  if len(ids) == 0:
    return False
  else:
    result = []
    for id in ids:
      result.append(construct_room(select_room(id)))
    return result

def construct_room(room):
  if len(room) == 0:
    return False
  else:
    result = {
      "numero": room[0],
      "nuitee": room[1],
      "presentation": room[2],
      "dispo": check_dispo(room[0])[0][0]
    }
  return result

def construct_comments(comments):
  if len(comments) == 0:
    return False
  else:
    result = []
    for comment in comments:
      result.append({
        "id": comment[0],
        "resa": comment[1],
        "contenu": comment[2],
        "date": comment[3],
        "auteur": " ".join(select_name_by_client(comment[6])[0])
      })
    return result

def construct_resa(resa):
  if resa is None:
    return False
  else:
    return {
      "id": resa[0],
      "id_chambre": resa[1],
      "id_client": resa[2],
      "date_debut": resa[3],
      "date_fin": resa[4],
      "paye": resa[5]
    }

def construct_resas(resas):
  if len(resas) == 0:
    return False
  else:
    result = []
    for resa in resas:
      result.append({
        "id": resa[0],
        "id_chambre": resa[1],
        "id_client": resa[2],
        "date_debut": resa[3],
        "date_fin": resa[4],
        "paye": resa[5]
      })
    return result

def construct_consos(consos):
  if len(consos) == 0:
    return False
  else:
    result = []
    for conso in consos:
      result.append({
        "id": conso[0],
        "consommation": conso[1],
        "tarif": conso[2]
      })
    return result

def construct_facture(facture):
  if facture is None:
    return False
  else:
    return {
      "id_resa": facture[0],
      "id_client": facture[1],
      "numero": facture[2],
      "prenom": facture[3],
      "nom": facture[4],
      "email": facture[5],
      "date_debut": facture[6],
      "date_fin": facture[7],
      "paye": facture[8],
      "nuitees": facture[9],
      "consos": facture[10],
      "total": facture[11]
    }

def construct_factures(factures):
  if len(factures) == 0:
    return False
  else:
    result = []
    for facture in factures:
      result.append({
        "id_resa": facture[0],
        "id_client": facture[1],
        "numero": facture[2],
        "prenom": facture[3],
        "nom": facture[4],
        "email": facture[5],
        "date_debut": facture[6],
        "date_fin": facture[7],
        "paye": facture[8],
        "nuitees": facture[9],
        "consos": facture[10],
        "total": facture[11]
      })
    return result

def construct_descs(descs):
  if len(descs) == 0:
    return False
  else:
    result = []
    for desc in descs:
      result.append({
        "id": desc[0],
        "description": desc[1]
      })
    return result
