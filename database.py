from flask import *
import psycopg2
import psycopg2.extras

class Database:

  def __init__(self, host, dbname, user, password, port=5432):
    self.ids = []
    self.ids.append("host="+host)
    self.ids.append("dbname="+dbname)
    self.ids.append("user="+user)
    self.ids.append("password="+password)
    self.ids.append("port="+str(port))

  def connect(self):
    self.db = psycopg2.connect(" ".join(self.ids))

  def dbinsert(self, command, args):
    cursor = self.db.cursor()
    try:
      cursor.execute(command, args)
      nb = cursor.rowcount
      self.db.commit()
      cursor.close()
      self.db.close()
      return nb
    except Exception as e:
      flash("impossible d'écrire dans la base de données...")
      return redirect(url_for("login", error=str(e)))

def dbconnect():
  db = Database(
    "localhost",
    "hotel2019",
    "root",
    "root",
    9876)
  db.connect()
  return db

def dbselect(command, args, uniq=False):
  db = dbconnect()
  cursor = db.db.cursor()
  cursor.execute(command, args)
  if not uniq:
    rows = cursor.fetchall()
  else:
    rows = cursor.fetchone()
  cursor.close()
  db.db.close()
  return rows
  
def dbinsert(command, args):
  db = dbconnect()
  cursor = db.db.cursor()
  cursor.execute(command, args)
  nb = cursor.rowcount
  db.db.commit()
  cursor.close()
  db.db.close()
  return nb

def insert_client(prenom, nom, email, password):
  return dbinsert("INSERT INTO hotel2019.Client (prenom, nom, email, pass) VALUES (%s, %s, %s, %s);", (prenom, nom, email, password))

def insert_comment(id_resa, content, date_ecriture):
  return dbinsert("INSERT INTO hotel2019.Comment (id_resa, content, date_ecriture) VALUES (%s, %s, %s);", (id_resa, content, date_ecriture))

def insert_resa(id_chambre, id_client, date_debut, date_fin):
  return dbinsert("INSERT INTO hotel2019.Reservation (id_chambre, id_client, date_debut, date_fin) VALUES (%s, %s, %s, %s);", (id_chambre, id_client, date_debut, date_fin))

def insert_conso(id_conso, id_resa, date_conso, nombre):
  return dbinsert("INSERT INTO hotel2019.Consommation (id_conso, id_resa, date_conso, nombre) VALUES (%s, %s, %s, %s);", (id_conso, id_resa, date_conso, nombre))

def pay_bill(id):
  return dbinsert("UPDATE hotel2019.Reservation SET paye = 'True' WHERE id = %s;", (id,))

def check_dispo(id_chambre):
  return dbselect("SELECT Hotel2019.ChambreDisponible(%s)", (id_chambre,))

def select_mails():
  return dbselect("SELECT email FROM hotel2019.Client", [])

def select_rooms():
  return dbselect("SELECT * FROM hotel2019.Chambre", [])

def select_room(id):
  return dbselect("SELECT * FROM hotel2019.Chambre WHERE numero = %s;", (id,), True)

def select_room_comments(id):
  return dbselect("SELECT * FROM hotel2019.Comment WHERE id_resa = %s;", (id,))

def select_comments_on_room(id):
  return dbselect("SELECT * FROM hotel2019.Comment INNER JOIN hotel2019.Reservation ON hotel2019.Comment.id_resa = hotel2019.Reservation.id WHERE hotel2019.Reservation.id_chambre = %s;", (id,))

def select_name_by_client(id):
  return dbselect("SELECT prenom,nom FROM hotel2019.Client WHERE hotel2019.Client.id = %s;", (id,))

def select_resa(id):
  return dbselect("SELECT * FROM Hotel2019.Reservation WHERE id = %s;", (id,), True)

def select_current_resa_on_room(id):
  return dbselect("SELECT * FROM hotel2019.Reservation WHERE hotel2019.Reservation.id_chambre = %s AND hotel2019.Reservation.date_debut <= NOW() AND hotel2019.Reservation.date_fin > NOW();", (id,), True)

def select_future_resas_on_room(id):
  return dbselect("SELECT * FROM hotel2019.Reservation WHERE hotel2019.Reservation.id_chambre = %s AND hotel2019.Reservation.date_fin > NOW();", (id,))

def select_resas_by_client(id):
  return dbselect("SELECT * FROM hotel2019.Reservation WHERE hotel2019.Reservation.id_client = %s;", (id,))

def select_current_resas_by_client(id):
  return dbselect("SELECT * FROM hotel2019.Reservation WHERE hotel2019.Reservation.id_client = %s AND hotel2019.Reservation.date_fin >= CURRENT_DATE;", (id,))

def select_free_rooms_on_period(date_debut, date_fin):
  return dbselect("SELECT numero FROM hotel2019.Chambre EXCEPT SELECT id_chambre FROM hotel2019.Reservation WHERE date_debut < %s AND date_fin > %s;", (date_fin, date_debut))

def select_consos():
  return dbselect("SELECT * FROM hotel2019.Bar;", [])

def select_facture(id):
  return dbselect("SELECT * FROM hotel2019.Facture WHERE id = %s;", (id,), True)

def select_factures(id_client):
  return dbselect("SELECT * FROM hotel2019.Facture WHERE id_client = %s;", (id_client,))

def del_resa(id, id_client):
  return dbinsert("DELETE FROM hotel2019.Reservation WHERE id = %s AND id_client = %s;", (id, id_client))

def select_consos_by_resa(id_resa):
  return dbselect("SELECT * FROM hotel2019.Consommation WHERE id_resa = %s;", (id_resa,))

def select_descs():
  return dbselect("SELECT * FROM hotel2019.Description;", [])

def connect_user(email, password):
  return dbselect("SELECT id,prenom,nom,email FROM hotel2019.Client WHERE email = %s AND pass = %s;", (email, password))
