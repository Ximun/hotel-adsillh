import time
import datetime
import hashlib
from flask import *
from database import *
from helpers import *
import sys

app = Flask(__name__)
app.secret_key = "some_secret"

"""
Page d'accueil
"""
@app.route("/")
def home():
  return render_template("home.html")

"""
Page de la description de l'hôtel
"""
@app.route("/hotel")
def hotel():
  descriptions = construct_descs(select_descs())
  return render_template("hotel.html", descriptions=descriptions)

"""
Page d'affichage des chambres.

  Si une plage de dates est entrée, elle affiche les chambres libres sur cette période.
  Sinon, toutes les pages sont affichées.
"""
@app.route("/chambres",methods=['GET','POST'])
def chambres():
  if not request.form:
    chambres = construct_rooms(select_rooms())
    period = {}
  else:
    chambres = construct_rooms_on_ids(select_free_rooms_on_period(request.form['from'], request.form['to']))
    debut = datetime.datetime.strptime(request.form['from'], "%Y-%m-%d").date()
    fin = datetime.datetime.strptime(request.form['to'], "%Y-%m-%d").date()
    period = {"debut":debut,"fin":fin}
  return render_template("chambres.html", chambres=chambres, period=period)

"""
Page d'affichage d'une chambre

  Pour une chambre:
  - affiche les commentaires
  - affiche les potentielles présentes ou futures réservations
"""
@app.route("/chambre/<int:id>")
def chambre(id):
  chambre = construct_room(select_room(id))
  comments = construct_comments(select_comments_on_room(id))
  futures = construct_resas(select_future_resas_on_room(id))
  
  if not chambre:
    flash("La chambre choisie n'existe pas.", "warning")
    return redirect(url_for("chambres"))
  return render_template("chambre.html", chambre=chambre, commentaires=comments, futures=futures)

"""
Page d'affichage des consommations disponibles

  Si le visiteur n'est pas inscrit, il peut seulement visualiser les consommations
  S'il est inscrit et a une réservation en cours, le formulaire de commande s'affiche
"""
@app.route("/bar")
def bar():
  consos = construct_consos(select_consos())
  if session:
    current_resas = construct_resas(select_current_resas_by_client(session['user']['id']))
  else:
    current_resas = None
  return render_template("bar.html", consos=consos, current_resas=current_resas)

"""
Affiche la page de login/inscription
"""
@app.route("/login")
def login():
  return render_template("login.html")

"""
Affiche l'espace personnel du client s'il est inscrit et connecté
"""
@app.route("/espace")
def admin():
  if not session:
    flash("Veuillez vous identifier d'abord", "warning")
    return redirect(url_for("login"))
  else:
    user = session['user']
    resas = construct_resas(select_resas_by_client(user['id']))
    current_resas = construct_resas(select_current_resas_by_client(user['id']))
    factures = construct_factures(select_factures(user['id']))
    return render_template("espace.html", user=user, resas=resas, current_resas=current_resas, factures=factures)


"""
Page d'affichage d'une facture, en fonction de l'id d'une réservation
"""
@app.route("/espace/facture/<int:id>")
def facture(id):
  facture = construct_facture(select_facture(id))
  return render_template("facture.html", facture=facture)

"""
Paye une facture en fonction de l'id de la réservation
"""
@app.route("/espace/facture/paye/<int:id>")
def paye(id):
  try:
    pay_bill(id)
    flash("Votre facture a bien été payée.", "success")
  except Exception as e:
    error("Impossible de payer la réservation.")
    error(str(e))
  return redirect(url_for("admin"))

"""
Annule une réservation

  Impossible si la date de début est dépassée
  Impossible si des consommations ont déjà été commandées (le premier jour)
"""
@app.route("/espace/annuler-reservation/<int:id>")
def delete_resa(id):
  resa = construct_resa(select_resa(id))
  consos = construct_consos(select_consos_by_resa(id))
  if datetime.date.today() > resa['date_debut']:
    error("Impossible d'annuler une réservation si la date de début est dépassée.")
  elif consos:
    error("Impossible d'annuler une réservation si vous avez déjà acheté des consommations.")
  else:
    del_resa(id, session['user']['id'])
    flash("Votre réservation a bien été annulée", "success")
  return redirect(url_for("admin"))

# RETOURS DE FORMULAIRES

"""
Inscrit un nouveau client en fonction du formulaire d'inscription

  - vérification de la confirmation du mot de passe
  - hashage du mot de passe
  - vérification de l'unicité de l'adresse mail
"""
@app.route("/inscription", methods=['POST'])
def post_inscription():
  prenom = request.form["signin-firstname"]
  nom = request.form["signin-lastname"]
  email = request.form["signin-email"]
  password = hashlib.md5(request.form["signin-password"].encode()).hexdigest()
  confirm = hashlib.md5(request.form["signin-password-confirm"].encode()).hexdigest()

  if password != confirm:
    flash("Vos mots de passe sont différents.", "warning")
  elif (email,) in select_mails():
    flash("Un utilisateur est déjà inscrit avec la même adresse e-mail.", "warning")
  else:
    try:
      insert_client(prenom, nom, email, password)
    except Exception as e:
      error("Inscription impossible")
      error(e)
      return redirect(url_for("login"))
    flash("Votre inscription s'est bien déroulée. Vous pouvez maintenant vous authentifier.", "success")
  return redirect(url_for("login"))

"""
Connecte et ouvre une session en fonction du formulaire de connexion
"""
@app.route("/connect", methods=['POST'])
def connect():
  email = request.form["connect-email"]
  password = hashlib.md5(request.form["connect-password"].encode()).hexdigest()

  try:
    user = construct_user(connect_user(email, password))
  except Exception as e:
    error("Connexion impossible.")
    error(str(e))

  if not user:
    flash("Aucun utilisateur n'est enregistré avec ces identifiants.", "warning")
    return redirect(url_for("login"))
  else:
    session['user'] = user
  return redirect(url_for("admin"))

"""
Location d'une chambre

  - vérifie que l'utilisateur est bien connecté
"""
@app.route("/location/<int:id>", methods=['POST'])
def location(id):
  if not session:
    flash("Merci de vous inscrire/authentifier au préalable.", "warning")
    return redirect(url_for("login"))
  else:
    id_chambre = id
    id_client = session['user']['id']
    date_debut = request.form['from']
    date_fin = request.form['to']

    try:
      insert_resa(id_chambre, id_client, date_debut, date_fin)
    except Exception as e:
      error("Inscription de la réservation impossible")
      error(e)
      return redirect(url_for("chambre",id=id))

    flash("Félicitations, votre chambre est réservée!", "success")
    return redirect(url_for("admin"))

"""
Poste un commentaire
"""
@app.route("/espace/comment", methods=['POST'])
def post_comment():
  id_resa = request.form["comment-resa"]
  comment = request.form["comment-area"]

  # TODO: Tester si id_resa appartient bien à la session user

  try:
    insert_comment(id_resa, comment, datetime.datetime.now())
  except Exception as e:
    error("Post du commentaire impossible.")
    error(str(e))
    return redirect(url_for("admin"))

  flash("Votre commentaire a bien été enregistré. Merci", "success")
  return redirect(url_for("admin"))

"""
Commande une ou plusieurs consommations en fonction d'une réservation en cours
"""
@app.route("/espace/conso", methods=['POST'])
def achat_conso():
  res = request.form.to_dict()
  empty = True
  for k,v in res.items():
    if k != 'current-resa' and int(v) > 0:
      empty = False
      try:
        insert_conso(k, request.form['current-resa'], datetime.date.today(), v)
      except Exception as e:
        error("Sauvegarde de la consommation impossible")
        error(e)
        return redirect(url_for("bar"))

  if empty:
    flash("Impossible de faire une commande vide.", "warning")
  else:
    flash("Vos consommations ont bien été enregistrées", "success")
  return redirect(url_for("bar"))

"""
Fermeture de session
"""
@app.route("/clear")
def clear():
  session.clear()
  return 'ok'

"""
Se déconnecter
"""
@app.route("/logout")
def logout():
  session.clear()
  flash("Vous êtes déconnecté.", "success")
  return redirect(url_for("login"))
